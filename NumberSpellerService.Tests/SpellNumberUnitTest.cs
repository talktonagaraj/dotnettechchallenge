﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberSpeller.Api.Utilities;

namespace NumberSpeller.Api.Tests
{
    [TestClass]
    public class SpellNumberUnitTest
    {
        [TestMethod]
        public void Check_Number_Is_Greater_Than_Maximum_Number_Allowed()
        {
            double input = 2147483647;
            string expectedResult = string.Format("Supports upto billion(.i.e. {0}) dollars only.", int.MaxValue);
            string actaulResult = Converter.SpellNumbers(input).Message;
            // assert  
            Assert.IsFalse(expectedResult.Equals(actaulResult), actaulResult);
        }

        [TestMethod]
        public void Zero_Number_Input()
        {
            double input = 0;
            string expectedResult = "ZERO DOLLARS";
            string actaulResult = Converter.SpellNumbers(input).Message;
            // assert  
            Assert.AreEqual(expectedResult, actaulResult);
        }

        [TestMethod]
        public void Decimal_Number_Input()
        {
            double input = 10.5;
            string expectedResult = "TEN DOLLARS AND FIVE CENTS";
            string actaulResult = Converter.SpellNumbers(input).Message;
            // assert 
            Assert.AreEqual(expectedResult, actaulResult);
        }

        [TestMethod]
        public void Negative_Number_Input()
        {
            double input = -10.5;
            string expectedResult = "NEGATIVE TEN DOLLARS AND FIVE CENTS";
            string actaulResult = Converter.SpellNumbers(input).Message;
            // assert  
            
            Assert.AreEqual(expectedResult, actaulResult);
        }

        [TestMethod]
        public void Maximum_Allowed_Fraction_Input()
        {
            double input = 10.101;
            string expectedResult = "Entered fraction number cannot be greater than 99";
            string actaulResult = Converter.SpellNumbers(input).Message;
            // assert  

            Assert.AreEqual(expectedResult, actaulResult);
        }

        [TestMethod]
        public void Test_Input_Thousands_Digit_Numbers()
        {
            int i = 0;
            double[] numbers = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 17, 19, 21, 32, 54, 76, 98,
                                100, 101, 211, 825, 999, 1000, 5000, 9999 };
            string[] expectedResult = { "ZERO DOLLARS", "ONE DOLLARS", "TWO DOLLARS", "THREE DOLLARS", "FOUR DOLLARS", "FIVE DOLLARS", "SIX DOLLARS", "SEVEN DOLLARS", "EIGHT DOLLARS", "NINE DOLLARS",
                                        "TEN DOLLARS", "ELEVEN DOLLARS", "TWELVE DOLLARS", "THIRTEEN DOLLARS", "SEVENTEEN DOLLARS", "NINETEEN DOLLARS",
                                        "TWENTY ONE DOLLARS", "THIRTY TWO DOLLARS", "FIFTY FOUR DOLLARS", "SEVENTY SIX DOLLARS", "NINETY EIGHT DOLLARS",
                                        "ONE HUNDRED DOLLARS", "ONE HUNDRED ONE DOLLARS", "TWO HUNDRED ELEVEN DOLLARS", "EIGHT HUNDRED TWENTY FIVE DOLLARS",
                                        "NINE HUNDRED NINETY NINE DOLLARS", "ONE THOUSAND DOLLARS", "FIVE THOUSAND DOLLARS", "NINE THOUSAND NINE HUNDRED NINETY NINE DOLLARS"
                                        };
            foreach (double input in numbers)
            {
                string s = Converter.SpellNumbers(input).Message;
                Assert.AreEqual(expectedResult[i++], s);
            }
        }

        [TestMethod]
        public void Test_Input_Greater_Than_Thousands_Digit_Numbers()
        {
            int i = 0;
            double[] numbers = { 100000.25, 8526598.85 };
            string[] expectedResult = { "ONE HUNDRED THOUSAND DOLLARS AND TWENTY FIVE CENTS",
                "EIGHT MILLION FIVE HUNDRED TWENTY SIX THOUSAND FIVE HUNDRED NINETY EIGHT DOLLARS AND EIGHTY FIVE CENTS" };
            foreach (double input in numbers)
            {
                string s = Converter.SpellNumbers(input).Message;
                Assert.AreEqual(expectedResult[i++], s);
            }
        }
    }
}

## Overview
This project consists of front end Angular application and back end .Net API service application.

## Prerequisites
Below are the list of prerequisites for both front end and back end applications.

### Back end API Service
- Windows 7 or later
- Visual Studio 2015 or later
- IIS express

### Front end application
- Nodejs V6.9.x
- npm V3.x.x

## How do I get set up? ###

To start up with setting and running application. Please follow below steps
#### Note: Make sure you follow the sequence order.
- Clone Git repository using https://talktonagaraj@bitbucket.org/talktonagaraj/dotnettechchallenge.git
- Pull the code from master branch.

### Back end application
- Open the solution file "NumberSpellerService.sln" from the root folder.
- Run Build Solution
- Run application using F5 (which will use IIS Express to run the application).
- You will observe browser window with URL http://localhost:62483/
- You could access API action from tool like Postman - Method: GET and URL: http://localhost:62483/api/spellnumber?number=8526598.85 

### Front end application
#### Note: Make sure Nodejs and npm is installed and Back end application is still running.
- Open CMD prompt window
- Change directory to "WORKINGFOLDER/FrontEndapp" using CD command line.
- Run command npm install -g @angular/cli
- Run command npm install
- Run command ng serve, at this time you are ready browse application by hitting URL http://localhost:4200/

### Automated Unit testing
- Open the solution file "NumberSpellerService.sln" from the root folder.
- Run All Test command using Keyboard Shortcut Ctrl + R, then press A. 



﻿using NumberSpeller.Api.Utilities;
using System.Text;
using System.Web.Http;

namespace NumberSpeller.Api.Controllers
{
    public class SpellNumberController : ApiController
    {
        /// <summary>
        /// Verb: GET
        /// API URL: /api/numberspeller/5
        /// </summary>
        /// <param name="number"></param>
        /// <returns>string</returns>
        public MessageResponse Get(double number)
        {
            return Converter.SpellNumbers(number);
        }
    }
}

﻿namespace NumberSpeller.Api.Utilities
{
    public static class Settings
    {
        public static string[] ThousandMoreInWords = { "", " thousand", " million", " billion", " trillion" };

        public static string[] TensInWords = { "", " ten", " twenty", " thirty", " forty", " fifty", " sixty", " seventy", " eighty", " ninety" };

        public static string[] NumbersInWords = { 
            "",
            " one",
            " two",
            " three",
            " four",
            " five",
            " six",
            " seven",
            " eight",
            " nine",
            " ten",
            " eleven",
            " twelve",
            " thirteen",
            " fourteen",
            " fifteen",
            " sixteen",
            " seventeen",
            " eighteen",
            " nineteen"
        };
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberSpeller.Api.Utilities
{
    public static class Converter
    {
        /// <summary>
        /// Spell Numbers
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static MessageResponse SpellNumbers(double number)
        {
            if (number >= int.MinValue && number <= int.MaxValue)
            {
                //Split the request number into integral and fractional parts
                string[] decimalParts = number.ToString().Split('.');
                StringBuilder words = new StringBuilder();
                //Get integral part
                if (decimalParts.Length >= 1)
                {
                    words.Append(Convert(int.Parse(decimalParts[0]))).Append(" dollars");
                }

                //Get fractional part
                if (decimalParts.Length == 2 && int.Parse(decimalParts[1]) > 0)
                {                 
                    if(int.Parse(decimalParts[1]) > 99)
                    {
                        return new MessageResponse { Message = "Entered fraction number cannot be greater than 99", Status = "Fail" };
                    }
                    words.Append(" and ").Append(Convert(int.Parse(decimalParts[1]))).Append(" cents");
                }

                return new MessageResponse { Message = words.ToString().ToUpper(), Status = "Success" };
            }

            return new MessageResponse { Message = string.Format("Supports upto billion(.i.e. {0}) dollars only.", int.MaxValue), Status = "Fail" };
        }

        /// <summary>
        /// Convert number to words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private static string Convert(int number)
        {

            if (number == 0) { return "zero"; }

            string prefix = "";

            if (number < 0)
            {
                number = -number;
                prefix = "negative";
            }

            string current = "";
            int place = 0;

            do
            {
                int n = number % 1000;
                if (n != 0)
                {
                    string s = ConvertLessThanOneThousand(n);
                    current = s + Settings.ThousandMoreInWords[place] + current;
                }
                place++;
                number /= 1000;
            } while (number > 0);

            return (prefix + current).Trim();
        }

        private static string ConvertLessThanOneThousand(int number)
        {
            string current;

            if (number % 100 < 20)
            {
                current = Settings.NumbersInWords[number % 100];
                number /= 100;
            }
            else
            {
                current = Settings.NumbersInWords[number % 10];
                number /= 10;

                current = Settings.TensInWords[number % 10] + current;
                number /= 10;
            }
            if (number == 0) return current;

            return Settings.NumbersInWords[number] + " hundred" + current;
        }
    }
}

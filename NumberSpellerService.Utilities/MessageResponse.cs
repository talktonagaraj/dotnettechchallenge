﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NumberSpeller.Api.Utilities
{
    /// <summary>
    /// MessageResponse
    /// </summary>
    public class MessageResponse
    {
        /// <summary>
        /// Message
        /// Returns: error message or numbers in words
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Status
        /// Returns: Success or Fail
        /// </summary>
        public string Status { get; set; }

    }
}
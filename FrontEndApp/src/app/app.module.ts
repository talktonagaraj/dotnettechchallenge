import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppService } from './services/app.service';
import { AppComponent } from './app.component';
import { SpellNumberComponent } from './spellnumber/spellnumber.component';

@NgModule({
  declarations: [
    AppComponent,
      SpellNumberComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';

@Injectable()
export class AppService {
    private url: string = "";
	private headers: Headers = environment.headers;
    private options = new RequestOptions({ headers: this.headers });

	constructor(private http: Http) {
        
    }
    
    GetNumberInWords(amount: number): Promise<any> {        
        let params =  {
            "number": amount
        };
        
        this.url = environment.apiURL + "/spellnumber?number=" + amount;
        
		return this.http.get(`${this.url}`)
            .toPromise()
			.then(response => response.json())
			.catch(err => alert(err));
	}
    
}
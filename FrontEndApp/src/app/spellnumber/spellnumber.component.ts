import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-spellnumber',
  templateUrl: './spellnumber.component.html'
})

export class SpellNumberComponent {

  name: string;
  amountInWords: string;
  isSuccessSubmit: boolean = false;
  constructor(private appService: AppService
  ) {

  }

  ngOnInit() {
      
      
  }
    
  onSubmit(input: number, name: string) {
      this.appService.GetNumberInWords(input).then(res => {
          console.log(res);
          if(res) {
              this.isSuccessSubmit = true;
              this.name = name;
              this.amountInWords = res.Message;
          }
      }).catch(err=> {
          this.isSuccessSubmit = false;
          this.amountInWords = "Technical difficulties";
      })
  }
}

import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SpellNumberComponent } from './spellnumber/spellnumber.component';


const routes: Routes = [
	{ path: '', redirectTo: 'numberspeller', pathMatch: 'full' },
    { path: 'numberspeller', component: SpellNumberComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { useHash: true }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

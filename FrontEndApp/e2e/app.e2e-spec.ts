import { MindlabPage } from './app.po';

describe('mindlab App', () => {
  let page: MindlabPage;

  beforeEach(() => {
    page = new MindlabPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
